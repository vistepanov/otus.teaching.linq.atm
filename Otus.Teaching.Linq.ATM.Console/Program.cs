﻿using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using static System.Console;

namespace Otus.Teaching.Linq.ATM.Console
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            WriteLine("Старт приложения-банкомата...");
            WriteLine();
            var atmManager = CreateATMManager();
            var menu = new AtmMenu(atmManager);
            var x = atmManager.GetAccounts(null);
            menu.MenuLoop();
            WriteLine("Завершение работы приложения-банкомата...");
        }

        public static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}