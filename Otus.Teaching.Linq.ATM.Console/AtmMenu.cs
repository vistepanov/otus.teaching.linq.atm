using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;

namespace Otus.Teaching.Linq.ATM.Console
{
    internal class AtmMenu
    {
        private ATMManager _atmManager;

        internal AtmMenu(ATMManager atmManager)
        {
            _atmManager = atmManager;
        }

        private static int PrintMenu()
        {
            System.Console.Clear();
            System.Console.WriteLine("Выберите:");
            System.Console.WriteLine("---------------------------------");
            System.Console.WriteLine("| 1 - Вход другого пользователя |");
            System.Console.WriteLine("| 2 - Список счетов             |");
            System.Console.WriteLine("| 3 - Движение по счетам        |");
            System.Console.WriteLine("| 4 - Все пополнения            |");
            System.Console.WriteLine("| 5 - Счета с остатком больше N |");
            System.Console.WriteLine("| 6 - Движение по всем счетам   |");
            System.Console.WriteLine("---------------------------------");
            System.Console.WriteLine("| 0 - Выход                     |");
            System.Console.WriteLine("---------------------------------");
            System.Console.Write("Сделайте выбор: ");
            var l = System.Console.ReadLine();
            if (int.TryParse(l, out int rv)) return rv;
            return int.MaxValue;
        }

        internal void MenuLoop()
        {
            var atmAuth = new AtmAuth(_atmManager);
            var user = atmAuth.ProcessLogin();
            do
            {
                var pause = true;
                var menuItem = PrintMenu();
                if (menuItem == 0)
                    break;
                switch (menuItem)
                {
                    case 1:
                        user = atmAuth.ProcessLogin();
                        break;
                    case 2:
                        PrintAccounts(user);
                        break;
                    case 3:
                        PrintMoveAccounts(user);
                        break;
                    case 4: // Все пополнения
                        PrintAllIncome();
                        break;
                    case 5:
                        System.Console.Write("Введите сумму: ");
                        var l = System.Console.ReadLine();
                        if (decimal.TryParse(l, out var sum))
                            PrintAccountsBySum(sum);
                        break;
                    case 6:
                        PrintMoveAccounts(null);
                        break;
                    default:
                        pause = false;
                        break;
                }

                if (pause)
                {
                    System.Console.WriteLine("press any key");
                    System.Console.ReadLine();
                }
            } while (true);
        }


        private void PrintAccounts(User user)
        {
            System.Console.WriteLine("Счета пользователя:");
            IEnumerable<Account> accounts = _atmManager.GetAccounts(user);
            foreach (var account in accounts)
            {
                System.Console.WriteLine(account);
            }
        }

        private void PrintMoveAccounts(User user)
        {
            IEnumerable<OperationsHistory> accountsHistory = _atmManager.GetAccountsHistory(user);
            foreach (var history in accountsHistory)
            {
                System.Console.WriteLine(history);
            }
        }

        private void PrintAllIncome()
        {
            IEnumerable<AllIncomes> incomes = _atmManager.GetAllIncomes();
            foreach (AllIncomes income in incomes)
            {
                System.Console.WriteLine(income.History.ToString() + " User: " + income.User.Login);
            }
        }

        private void PrintAccountsBySum(decimal sum)
        {
            IEnumerable<AccountSum> details = _atmManager.GetAllAccountWithSum(sum);
            foreach (var detail in details)
            {
                System.Console.WriteLine($"Пользователь - {detail.User.Id}, {detail.User.Login}. Остаток {detail.Sum}");
            }
        }
    }
}