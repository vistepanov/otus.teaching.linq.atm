using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;

namespace Otus.Teaching.Linq.ATM.Console
{
    internal class AtmAuth
    {
        private ATMManager _atmManager;

        public AtmAuth(ATMManager atmManager)
        {
            _atmManager = atmManager;
        }

        internal User ProcessLogin()
        {
            User user;
            do
            {
                user = Login();
            } while (user == null);

            System.Console.WriteLine();
            System.Console.WriteLine("Пользователь найден.");
            System.Console.WriteLine(user);
            return user;
        }

        private User Login()
        {
            System.Console.WriteLine();
            System.Console.Write("Введите логин: ");
            var login = System.Console.ReadLine();
            System.Console.Write("Введите пароль: ");
            var pwd = System.Console.ReadLine();
            return _atmManager.LoginUser(login, pwd);
        }
    }
}