﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class Account
    {
        public override string ToString()
        {
            return $"Счёт №{Id}, Открыт {OpeningDate.ToShortDateString()}. Остаток: {CashAll}";
        }

        public int Id { get; set; }
        public DateTime OpeningDate { get; set; }
        public decimal CashAll { get; set; }
        public int UserId { get; set; }
    }
}