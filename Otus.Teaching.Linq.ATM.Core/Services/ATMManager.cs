﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата
        public User LoginUser(string login, string pwd) =>
            Users.FirstOrDefault(u => u.Login == login && u.Password == pwd);

        public IEnumerable<Account> GetAccounts(User user) =>
            Accounts.Where(acc => acc.UserId == user.Id);


        public IEnumerable<OperationsHistory> GetAccountsHistory(User user)
        {
            return History
                .Join(Accounts.Where(ac => user == null || ac.Id == user.Id),
                    history => history.AccountId,
                    account => account.Id,
                    (history, account) =>
                        history
                );
        }

        public IEnumerable<AllIncomes> GetAllIncomes()
        {
            var x = from history in History
                join account in Accounts on history.AccountId equals account.Id
                join user in Users on account.UserId equals user.Id
                where history.OperationType == OperationType.InputCash
                select new AllIncomes {History = history, User = user};
            return x;
        }

        public IEnumerable<AccountSum> GetAllAccountWithSum(decimal sum)
        {
            return Users
                .GroupJoin(Accounts,
                    user => user.Id,
                    account => account.UserId,
                    (user, accounts) =>
                        new
                        {
                            User = user,
                            Amount = accounts.Sum(s => s.CashAll)
                        }
                )
                .Where(o=>o.Amount>sum)
                .Select(o=>new AccountSum {User = o.User, Sum = sum});
        }
    }

    public class AllIncomes
    {
        public OperationsHistory History { get; set; }
        public User User { get; set; }
    }

    public class AccountSum
    {
        public User User { get; set; }
        public decimal Sum { get; set; }
    }
}