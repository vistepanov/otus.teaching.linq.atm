using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Console;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Xunit;

namespace Otus.Teaching.Linq.ATM.Tests
{
    public class AtmManagerTest
    {
        private readonly ATMManager _atmManager;

        public AtmManagerTest()
        {
            _atmManager = Program.CreateATMManager();
        }

        [Fact]
        public void LoginCorrect()
        {
            User user = _atmManager.LoginUser("cop", "555");
            Assert.Equal(5, user.Id);
        }

        [Fact]
        public void LoginWrongUser()
        {
            User user = _atmManager.LoginUser("cap", "555");
            Assert.Null(user);
        }

        [Fact]
        public void LoginWrongPwd()
        {
            User user = _atmManager.LoginUser("cop", "55");
            Assert.Null(user);
        }

        [Fact]
        public void AccountsCount()
        {
            User user = _atmManager.LoginUser("cop", "555");
            IEnumerable<Account> accounts = _atmManager.GetAccounts(user);
            Assert.Equal(2, accounts.Count());
        }

        [Fact]
        public void AccountNullUser()
        {
            var accounts = _atmManager.GetAccounts(null);
            Assert.ThrowsAny<Exception>(() => accounts.Count());
        }

    }
}